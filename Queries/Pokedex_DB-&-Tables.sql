create database Pokemon

/***************************************/

Use Pokemon;
Go
create table Pokemon (
ID int not null primary key identity(1, 1),
Name varchar(255)
);
Go
create table Abilities (
ID int not null primary key identity(1, 1),
Name varchar(255)
);
Go
create table Berries (
ID int not null primary key identity(1, 1),
Name varchar(255)
);