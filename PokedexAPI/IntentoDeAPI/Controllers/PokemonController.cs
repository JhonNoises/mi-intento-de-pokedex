﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntentoDeAPI.Queries_Manager;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IntentoDeAPI.Controllers
{
	[ApiController]
	[Route("[controller]")]
	public class PokemonController : ControllerBase
	{
		private readonly ILogger<PokemonController> _logger;

		public PokemonController(ILogger<PokemonController> logger)
		{
			_logger = logger;
		}

		[Route("getFilteredPokemonNames/{term}")]
		[HttpGet]
		public async Task<IEnumerable<string>> GetFilteredPokemonNames(string term)
		{
			if (!string.IsNullOrWhiteSpace(term) && term.Length >= 2)
			{
				return await Queries.GetFilteredPokemonNames(term.ToLowerInvariant());
			}
			else
			{
				return new List<string>();
			}
		}

		[Route("getFilteredAbilityNames/{term}")]
		[HttpGet]
		public async Task<IEnumerable<string>> GetFilteredAbilityNames(string term)
		{
			if (!string.IsNullOrWhiteSpace(term) && term.Length >= 2)
			{
				return await Queries.GetFilteredAbilityNames(term.ToLowerInvariant());
			}
			else
			{
				return new List<string>();
			}
		}

		[Route("getFilteredBerryNames/{term}")]
		[HttpGet]
		public async Task<IEnumerable<string>> GetFilteredBerryNames(string term)
		{
			if (!string.IsNullOrWhiteSpace(term) && term.Length >= 2)
			{
				return await Queries.GetFilteredBerryNames(term.ToLowerInvariant());
			}
			else
			{
				return new List<string>();
			}
		}
	}
}
