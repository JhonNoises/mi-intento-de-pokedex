﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IntentoDeAPI.Queries_Manager
{
	public static class Queries
	{
		private static readonly string connectionString = "Data Source=JHONNOISES\\JHONSMEMORIES;Initial Catalog=Pokemon;Integrated Security=True";

		/// <summary>
		/// Gets the filtered pokemon names, filtering by the given <paramref name="searchTerm"/>.
		/// </summary>
		/// <param name="searchTerm">Search term to filter by.</param>
		/// <returns>Filtered pokemon names.</returns>
		public static async Task<List<string>> GetFilteredPokemonNames(string searchTerm)
		{
			var filteredPokemonNames = new List<string>();
			using (var conn = new SqlConnection(connectionString))
			{
				using (var command = new SqlCommand($"select Name from Pokemon (nolock) where Name like '{searchTerm}%'", conn))
				{
					try
					{
						conn.Open();
						using (var reader = await command.ExecuteReaderAsync())
						{
							while (reader.Read())
							{
								filteredPokemonNames.Add(reader.GetString(0));
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
			}

			return filteredPokemonNames;
		}

		/// <summary>
		/// Gets the filtered berry names, filtering by the given <paramref name="searchTerm"/>.
		/// </summary>
		/// <param name="searchTerm">Search term to filter by.</param>
		/// <returns>Filtered berry names.</returns>
		public static async Task<List<string>> GetFilteredBerryNames(string searchTerm)
		{
			var filteredBerryNames = new List<string>();
			using (var conn = new SqlConnection(connectionString))
			{
				using (var command = new SqlCommand($"select Name from Berries (nolock) where Name like '{searchTerm}%'", conn))
				{
					try
					{
						conn.Open();
						using (var reader = await command.ExecuteReaderAsync())
						{
							while (reader.Read())
							{
								filteredBerryNames.Add(reader.GetString(0));
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
			}

			return filteredBerryNames;
		}

		/// <summary>
		/// Gets the filtered ability names, filtering by the given <paramref name="searchTerm"/>.
		/// </summary>
		/// <param name="searchTerm">Search term to filter by.</param>
		/// <returns>Filtered ability names.</returns>
		public static async Task<List<string>> GetFilteredAbilityNames(string searchTerm)
		{
			var filteredAbilityNames = new List<string>();
			using (var conn = new SqlConnection(connectionString))
			{
				using (var command = new SqlCommand($"select Name from Abilities (nolock) where Name like '{searchTerm}%'", conn))
				{
					try
					{
						conn.Open();
						using (var reader = await command.ExecuteReaderAsync())
						{
							while (reader.Read())
							{
								filteredAbilityNames.Add(reader.GetString(0));
							}
						}
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex.Message);
					}
				}
			}

			return filteredAbilityNames;
		}
	}
}